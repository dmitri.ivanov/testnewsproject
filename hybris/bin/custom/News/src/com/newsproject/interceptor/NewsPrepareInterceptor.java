package com.newsproject.interceptor;

import com.newsproject.model.TestNewsItemModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class NewsPrepareInterceptor implements PrepareInterceptor<TestNewsItemModel> {
    @Autowired
    private KeyGenerator newsCodeGenerator;

    @Override
    public void onPrepare(TestNewsItemModel model, InterceptorContext ctx) throws InterceptorException {
        if (StringUtils.isEmpty(model.getId())) {
            model.setId(createId());
        }
    }

    private String createId() {
        return this.newsCodeGenerator.generate().toString();
    }
}
