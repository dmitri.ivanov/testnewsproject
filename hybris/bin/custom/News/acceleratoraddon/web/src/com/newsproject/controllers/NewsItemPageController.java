package com.newsproject.controllers;


import com.newsproject.breadcrumbs.TestNewsPageBreadcrumbBuilder;
import com.newsproject.data.TestNewsItemData;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.addonsupport.controllers.page.AbstractAddOnPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.newsproject.facades.facade.TestNewsItemFacade;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/testnews")
public class NewsItemPageController extends AbstractAddOnPageController {

    private static final String NEWS_ITEM_PATH_VARIABLE_PATTERN = "/{code:.*}";
    private static final String ERROR_CMS_PAGE = "notFound";
    private static final String NEWS_ITEM_PAGE = "testNewsItem";
    private static final String NEWS_ROOT_PAGE = "testNewsItem";
    private static final String NEWS_URL = "/news";

    @Resource(name = "testNewsItemFacade")
    private TestNewsItemFacade testNewsItemFacade;

    @Autowired
    private TestNewsPageBreadcrumbBuilder testNewsPageBreadcrumbBuilder;

    @RequestMapping(method = RequestMethod.GET)
    public String showNews(final Model model) throws CMSItemNotFoundException {
        ContentPageModel contentPageModel = getContentPageForLabelOrId(NEWS_ROOT_PAGE);
        storeCmsPageInModel(model, contentPageModel);
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(NEWS_ROOT_PAGE));
        List<Breadcrumb> breadcrumbs = new ArrayList<>();
        breadcrumbs.add(new Breadcrumb(NEWS_URL, contentPageModel.getName(), "active"));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
        model.addAttribute("newsRootItems", testNewsItemFacade.getAllNews());
        return "addon:/News/pages/testNewsItemPage";
    }

    @RequestMapping(value = NEWS_ITEM_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String viewNewsItemPage(@PathVariable("code") final String code, final Model model,
                                   final HttpServletResponse response) throws CMSItemNotFoundException {
        TestNewsItemData newsItem = testNewsItemFacade.getNewsItem(code);

        if (newsItem == null) {
            return getNotFoundPage(model, response);
        }
        model.addAttribute("newsItem", newsItem);
        ContentPageModel contentPageModel = getContentPageForLabelOrId(NEWS_ITEM_PAGE);
        storeCmsPageInModel(model, contentPageModel);
        setUpMetaDataForContentPage(model,
                getContentPageForLabelOrId(NEWS_ITEM_PAGE));
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getBreadcrumbs(newsItem, contentPageModel.getName()));
        return "addon:/News/pages/testNewsItemPage";
    }

    private String getNotFoundPage(final Model model, final HttpServletResponse response)
            throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(ERROR_CMS_PAGE));
        ContentPageModel contentPageModel = getContentPageForLabelOrId(ERROR_CMS_PAGE);
        setUpMetaDataForContentPage(model, contentPageModel);

        model.addAttribute(WebConstants.BREADCRUMBS_KEY, testNewsPageBreadcrumbBuilder.getBreadcrumbs(contentPageModel));
        GlobalMessages.addErrorMessage(model, "system.error.page.not.found");

        response.setStatus(HttpServletResponse.SC_NOT_FOUND);

        return "addon:/News/pages/errorNotFoundPage";
    }

    private List<Breadcrumb> getBreadcrumbs(TestNewsItemData news, String pageName) {
        List<Breadcrumb> breadcrumbs = new ArrayList<>();
        breadcrumbs.add(new Breadcrumb(NEWS_URL, pageName, "active"));
        breadcrumbs.add(new Breadcrumb(NEWS_URL + "/" + news.getId(), news.getTitle(), "active"));
        return breadcrumbs;
    }

}
