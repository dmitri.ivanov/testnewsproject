package com.newsproject.facades.facade.impl;

import com.newsproject.data.TestNewsItemData;
import com.newsproject.model.TestNewsItemModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.newsproject.facades.facade.TestNewsItemFacade;
import com.newsproject.service.TestNewsService;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

public class TestNewsItemFacadeImpl implements TestNewsItemFacade {

    @Resource(name = "testNewsItemConverter")
    private Converter<TestNewsItemModel, TestNewsItemData> testNewsItemConverter;

    @Resource(name = "testNewsService")
    private TestNewsService testNewsService;

    @Override
    public TestNewsItemData getNewsItem(String code) {
        return StringUtils.isNotEmpty(code) ? testNewsItemConverter.convert(testNewsService.getNewsItemByCode(code)) : null;
    }

    @Override
    public List<TestNewsItemData> getNewsItems(List<TestNewsItemModel> newsItemModelList) {
        return CollectionUtils.isNotEmpty(newsItemModelList) ? Converters.convertAll(newsItemModelList, testNewsItemConverter) : Collections.emptyList();
    }

    @Override
    public List<TestNewsItemData> getAllNews() {
        List<TestNewsItemModel> allNews = testNewsService.getAllNews();
        return CollectionUtils.isNotEmpty(allNews) ? Converters.convertAll(allNews, testNewsItemConverter) : Collections.emptyList();
    }
}