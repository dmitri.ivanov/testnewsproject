<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

News List View
<c:choose>
    <c:when test="${not empty newsItems}">
        <c:forEach var="newsItem" items="${newsItems}">
            <div>
                News Preview<br><br>
                Published: ${newsItem.publishStartDate}<br>
                Title: ${newsItem.title}<br>
                Picture preview: <br>
                <img src="${newsItem.pictureUrl}" width="300" height="200"/><br>
                <a href="/news/${newsItems.id}">Read more...</a>
            </div>
        </c:forEach>
    </c:when>
</c:choose>
