package com.newsproject.dao;





import com.newsproject.model.TestNewsItemModel;
import java.util.Date;
import java.util.List;

public interface TestNewsDao {

    TestNewsItemModel findNewsItemsByCode(String code);

    List<TestNewsItemModel> findAllNewsItems();

    List<TestNewsItemModel> findNewsItemsByClient(String clientName);

    List<TestNewsItemModel> findNewsItemsByProduct(String productName);

    List<TestNewsItemModel> findNewsItemsByDateRange(Date publishStartDate, Date publishEndDate);


}
