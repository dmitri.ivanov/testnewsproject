/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.newsproject;

/**
 */
public interface NewsControllerConstants
{
	// implement here controller constants used by this extension
}
