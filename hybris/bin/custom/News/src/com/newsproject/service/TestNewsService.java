package com.newsproject.service;




import com.newsproject.model.TestNewsItemModel;

import java.util.Date;
import java.util.List;

public interface TestNewsService {

     List<TestNewsItemModel> getAllNews();

     List<TestNewsItemModel> getNewsItemsByProduct(String productName);

     TestNewsItemModel getNewsItemByCode(String code);

     List<TestNewsItemModel> getNewsItemsByClient(String clientName);

     List<TestNewsItemModel> getNewsItemsByDateRange(Date publishStartDate, Date publishEndDate);

}
