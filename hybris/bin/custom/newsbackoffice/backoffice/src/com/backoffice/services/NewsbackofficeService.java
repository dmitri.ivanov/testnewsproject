/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.backoffice.services;

/**
 * Hello World NewsbackofficeService
 */
public class NewsbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
