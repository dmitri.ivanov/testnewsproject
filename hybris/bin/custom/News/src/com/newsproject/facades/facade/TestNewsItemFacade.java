package com.newsproject.facades.facade;


import com.newsproject.data.TestNewsItemData;
import com.newsproject.model.TestNewsItemModel;


import java.util.List;

public interface TestNewsItemFacade {

    TestNewsItemData getNewsItem(String id);

    List<TestNewsItemData> getNewsItems(List<TestNewsItemModel> newsItemModelList);

    List<TestNewsItemData> getAllNews();

}
