package com.newsproject.facades.populators;


import com.newsproject.data.TestNewsItemData;
import com.newsproject.model.TestNewsItemModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


import java.text.SimpleDateFormat;

public class TestNewsItemPopulator implements Populator<TestNewsItemModel, TestNewsItemData> {

    @Override
    public void populate(TestNewsItemModel source, TestNewsItemData target) throws ConversionException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        target.setPublishStartDate(source.getPublishStartDate() != null ? formatter.format(source.getPublishStartDate()) : null);
        target.setPublishEndDate(source.getPublishEndDate() != null ? formatter.format(source.getPublishEndDate()) : null);
        target.setTitle(source.getTitle());
        target.setHtmlContent(source.getHtmlContent());
        target.setPictureUrl(source.getPicture().getURL());
        target.setId(source.getId());
    }
}
