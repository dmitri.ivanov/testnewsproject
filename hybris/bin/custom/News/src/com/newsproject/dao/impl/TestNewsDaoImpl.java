package com.newsproject.dao.impl;



import com.newsproject.dao.TestNewsDao;

import com.newsproject.model.TestNewsItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.List;

public class TestNewsDaoImpl extends AbstractItemDao implements TestNewsDao {

    private static final String GET_NEWS_BY_CODE = "SELECT {" + TestNewsItemModel.PK + "} from {"
            + TestNewsItemModel._TYPECODE + "} where {" + TestNewsItemModel.ID + "}=?id and {"
            + TestNewsItemModel.STATUS + "}=?status";

    private static final String GET_NEWS_BY_PRODUCT  = "select { ni." + TestNewsItemModel.PK + "} from {"
            + TestNewsItemModel._TYPECODE + " as ni JOIN " + ProductModel._TYPECODE
            + " as p ON {ni.product} = {p.pk}} where {p." + ProductModel.NAME + "}=?name and {"
            + TestNewsItemModel.STATUS + "}=?status";

    private static final String GET_All_NEWS = "select {" + TestNewsItemModel.PK + "} from {"
            + TestNewsItemModel._TYPECODE + "} where {" + TestNewsItemModel.STATUS + "}=?status";

    private static final String GET_NEWS_BY_DATE_RANGE = "select {" + TestNewsItemModel.PK + "} from {"
            + TestNewsItemModel._TYPECODE + "} where {" + TestNewsItemModel.PUBLISHSTARTDATE
            + "} >=?publishStartDate and {" + TestNewsItemModel.PUBLISHENDDATE + "} >=?publishEndDate and {"
            + TestNewsItemModel.STATUS + "}=?status";

    @Override
    public TestNewsItemModel findNewsItemsByCode(String id) {
        FlexibleSearchQuery query = new FlexibleSearchQuery(GET_NEWS_BY_CODE);
        query.addQueryParameter("id", id);
        query.addQueryParameter("status", true);

        SearchResult<TestNewsItemModel> result = getFlexibleSearchService().search(query);
        if (result.getCount() == 0) {
            return null;
        }
        return result.getResult().iterator().next();
    }

    @Override
    public List<TestNewsItemModel> findAllNewsItems() {
        FlexibleSearchQuery query = new FlexibleSearchQuery(GET_All_NEWS);
        query.addQueryParameter("status", true);

        SearchResult<TestNewsItemModel> result = getFlexibleSearchService().search(query);
        if (result.getCount() == 0) {
            return null;
        }
        return result.getResult();
    }

    @Override
    public List<TestNewsItemModel> findNewsItemsByClient(String clientName) {
        return null;
    }

    @Override
    public List<TestNewsItemModel> findNewsItemsByProduct(String productName) {
        FlexibleSearchQuery query = new FlexibleSearchQuery(GET_NEWS_BY_PRODUCT);
        query.addQueryParameter("name", productName);
        query.addQueryParameter("status", true);
        SearchResult<TestNewsItemModel> result = getFlexibleSearchService().search(query);
        if (result.getCount() == 0) {
            return null;
        }
        return result.getResult();
    }

    @Override
    public List<TestNewsItemModel> findNewsItemsByDateRange(Date publishStartDate, Date publishEndDate) {
        FlexibleSearchQuery query = new FlexibleSearchQuery(GET_NEWS_BY_DATE_RANGE);
        query.addQueryParameter("publishStartDate", publishStartDate);
        query.addQueryParameter("publishEndDate", publishEndDate);
        query.addQueryParameter("status", true);
        SearchResult<TestNewsItemModel> result = getFlexibleSearchService().search(query);
        if (result.getCount() == 0) {
            return null;
        }
        return result.getResult();
    }
}
