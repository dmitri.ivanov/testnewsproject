package com.newsproject.service.impl;



import com.newsproject.model.TestNewsItemModel;
import com.newsproject.service.TestNewsService;
import com.newsproject.dao.TestNewsDao;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

public class DefaultTestNewsService implements TestNewsService {

    @Resource
    private TestNewsDao testNewsDao;

    @Override
    public List<TestNewsItemModel> getNewsItemsByProduct(String productName) {
        return testNewsDao.findNewsItemsByProduct(productName);
    }

    @Override
    public TestNewsItemModel getNewsItemByCode(String code) {
        return testNewsDao.findNewsItemsByCode(code);
    }

    @Override
    public List<TestNewsItemModel> getNewsItemsByClient(String clientName) {
        return testNewsDao.findNewsItemsByClient(clientName);
    }

    @Override
    public List<TestNewsItemModel> getNewsItemsByDateRange(Date publishStartDate, Date publishEndDate) {
        return testNewsDao.findNewsItemsByDateRange(publishStartDate, publishEndDate);
    }

    @Override
    public List<TestNewsItemModel> getAllNews() {
        return testNewsDao.findAllNewsItems();
    }
}
