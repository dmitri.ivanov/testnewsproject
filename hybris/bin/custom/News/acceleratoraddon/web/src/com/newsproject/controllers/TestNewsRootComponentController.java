package com.newsproject.controllers;



import com.newsproject.data.TestNewsItemData;
import com.newsproject.model.TestNewsItemComponentModel;
import com.newsproject.model.TestNewsItemModel;
import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.newsproject.facades.facade.TestNewsItemFacade;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

@Controller("TestNewsRootComponentController")
@RequestMapping("/view/TestNewsRootComponentController")
public class TestNewsRootComponentController extends AbstractCMSAddOnComponentController<TestNewsItemComponentModel> {

    @Resource(name = "newsItemFacade")
    private TestNewsItemFacade newsItemFacade;

    @Override
    protected void fillModel(HttpServletRequest request, Model model, TestNewsItemComponentModel component) {
        final List<TestNewsItemModel> newsItemModelList = component.getTestNewsItems();
        if (newsItemModelList != null) {
            List<TestNewsItemData> newsItemDataList = newsItemFacade.getNewsItems(newsItemModelList);
            model.addAttribute("newsItems", newsItemDataList);
        } else {
            model.addAttribute("newsItems", Collections.emptyList());
        }
    }

    @Override
    protected String getView(TestNewsItemComponentModel component) {

        return "addon:/News/cms/newslistcomponent";
    }
}
