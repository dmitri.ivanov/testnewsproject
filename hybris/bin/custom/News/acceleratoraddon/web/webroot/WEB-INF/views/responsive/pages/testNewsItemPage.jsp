<%@ page trimDirectiveWhitespaces="true" contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<template:page pageTitle="${pageTitle}">
    <div id="breadcrumb" class="breadcrumb">
        <breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
    </div>
    <div id="globalMessages"><common:globalMessages/></div>
    <div class="infoPage">
        <div class="infoPage--title">${newsItem.title}</div>
        <div class="infoPage--date">${newsItem.publishDate}</div>
        <article>
                ${newsItem.text}
        </article>
    </div>
</template:page>