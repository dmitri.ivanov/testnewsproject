package com.newsproject.jalo;

import com.newsproject.constants.NewsConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class NewsManager extends GeneratedNewsManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( NewsManager.class.getName() );
	
	public static final NewsManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (NewsManager) em.getExtension(NewsConstants.EXTENSIONNAME);
	}
	
}
