package com.newsproject.controllers;



import com.newsproject.data.TestNewsItemData;
import com.newsproject.facades.facade.TestNewsItemFacade;
import com.newsproject.model.TestNewsItemComponentModel;
import com.newsproject.model.TestNewsItemModel;
import de.hybris.platform.addonsupport.controllers.cms.AbstractCMSAddOnComponentController;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller("TestNewsItemComponentController")
@RequestMapping("/view/TestNewsItemComponentController")
public class TestNewsItemComponentController extends AbstractCMSAddOnComponentController<TestNewsItemComponentModel> {

    @Resource(name = "newsItemFacade")
    private TestNewsItemFacade newsItemFacade;

    @Override
    protected void fillModel(HttpServletRequest request, Model model, TestNewsItemComponentModel component) {
        final List<TestNewsItemModel> newsItemModels = component.getTestNewsItems();
        if (CollectionUtils.isNotEmpty(newsItemModels)) {
            TestNewsItemData newsItemData = newsItemFacade.getNewsItem(newsItemModels.get(0).getId());
            model.addAttribute("newsItem", newsItemData);
        }
    }

    @Override
    protected String getView(TestNewsItemComponentModel component) {
        return "";
    }
}
