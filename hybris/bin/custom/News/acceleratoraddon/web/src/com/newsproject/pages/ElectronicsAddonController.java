package com.newsproject.pages;
  
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cmsfacades.data.ProductData;
import de.hybris.platform.commercefacades.product.ProductFacade;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping(value = "/")
public class ElectronicsAddonController
{
    @Resource(name = "ProductFacade")
    private ProductFacade ProductFacade;

    private static final Logger LOG = Logger.getLogger(ElectronicsAddonController.class);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getProductDetails(final Model model) throws CMSItemNotFoundException
    {
        Breadcrumb breadcrumb = new Breadcrumb("", "", "");
        LOG.info("########## ProductDetailsController updateOldPassword() method #####");

        final List<ProductData> ProductData = Collections.singletonList(new ProductData());

        model.addAttribute("ProductData", ProductData);

        return "addon:/News/pages/electronicsproductdetails";
    }
}